//switch between 2 background images
function mode() {
    let btn = document.getElementById("mode");
    let button = btn.innerHTML;
    if(button == "Duck Mode") {
        document.getElementById("bg").style.backgroundImage = "url(/images/duck.jpg)";
        btn.innerHTML = "Quack Mode";
    }
    else {
        document.getElementById("bg").style.backgroundImage = "url(/images/stoffeln.jpg)";
        btn.innerHTML = "Duck Mode";
    }
}