//import http from 'http'
import fs from 'fs'
import express from 'express'
const app = express();

app.use(express.static("./../frontend"));
app.use(express.json({ extended: true }));

app.post('/', function(req, res) {
  fs.readFile('./../frontend/assets/json/message.json', function(err, message) {
    var oldMessage = JSON.parse(message);
    let neww = req.body.input;
    console.log(req.body);
    console.log(neww);
    oldMessage.push(neww);
    var newMessage = JSON.stringify(oldMessage);
    console.log(newMessage);
    fs.writeFile('./../frontend/assets/json/message.json', newMessage, err => {
      if(err) throw err;
      console.log("New data added");
    })
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(newMessage);
    res.end();
  });
});
app.listen(8080);

/*http.createServer(function (req, res) {
  fs.readFile('./../frontend/assets/json/message.json', function(err, message) {
    var oldMessage = JSON.parse(message);
    let neww = "test";
    oldMessage.push(neww);
    var newMessage = JSON.stringify(oldMessage);
    fs.writeFile('./../frontend/assets/json/message.json', newMessage, err => {
      if(err) throw err;
      console.log("New data added");
    })
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(newMessage);
    res.end();
  });
}).listen(8080);*/