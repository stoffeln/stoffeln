import express from 'express'
import path from 'path'
import fs from 'fs'

const app = express();

app.use(express.static('src/frontend/assets'));
app.use(express.json({ extended: true }));

app.get('/', (req, res) => {
  const filePath = path.resolve('src/frontend/index.html');
  res.sendFile(filePath);
});

app.post('/', function(req, res) {
  fs.readFile('/json/message.json', function(err, message) {
    var oldMessage = JSON.parse(message);
    let neww = req.body.input;
    console.log(req.body);
    console.log(neww);
    oldMessage.push(neww);
    var newMessage = JSON.stringify(oldMessage);
    console.log(newMessage);
    fs.writeFile('/json/message.json', newMessage, err => {
      if(err) throw err;
      console.log("New data added");
    })
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(newMessage);
    res.end();
  });
});

app.listen(8080, function() {
    console.log('Server startet on port 8080')
});